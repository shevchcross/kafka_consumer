package com.conduent.databroker.kafka_consumer.kafka;


import com.conduent.databroker.kafka_consumer.model.ProcessedHit;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;


public class Listener {


    @KafkaListener(topics = "EventHitInput", containerFactory = "hitKafkaListenerContainerFactory")
    public void hitListener1(ProcessedHit processedHit) {
        System.out.println(processedHit);
        System.out.println(processedHit.getCallback());
        }




}
