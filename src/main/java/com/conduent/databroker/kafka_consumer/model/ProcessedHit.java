package com.conduent.databroker.kafka_consumer.model;

import java.math.BigDecimal;
import java.util.Date;

public class ProcessedHit {
//    private String transactionId;
//    private long brokerUserId;
//    private long clientId;
//    private int agencyId;
//    private String firstName;
//    private String lastName;
//    private int gantryId;
   // private Date timestamp;
    private String direction;
    private String callback;
   // private BigDecimal priceCharged;

    public ProcessedHit() {
    }

//    public ProcessedHit(String transactionId, long brokerUserId, long clientId, int agencyId,
//                        String firstName, String lastName, int gantryId, Date timestamp,
//                        String direction, String callback, BigDecimal priceCharged) {
//        this.transactionId = transactionId;
//        this.brokerUserId = brokerUserId;
//        this.clientId = clientId;
//        this.agencyId = agencyId;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.gantryId = gantryId;
//        this.timestamp = timestamp;
//        this.direction = direction;
//        this.callback = callback;
//        this.priceCharged = priceCharged;
//    }
//
//    public String getTransactionId() {
//        return transactionId;
//    }
//
//    public void setTransactionId(String transactionId) {
//        this.transactionId = transactionId;
//    }
//
//    public long getBrokerUserId() {
//        return brokerUserId;
//    }
//
//    public void setBrokerUserId(long brokerUserId) {
//        this.brokerUserId = brokerUserId;
//    }
//
//    public long getClientId() {
//        return clientId;
//    }
//
//    public void setClientId(long clientId) {
//        this.clientId = clientId;
//    }
//
//    public int getAgencyId() {
//        return agencyId;
//    }
//
//    public void setAgencyId(int agencyId) {
//        this.agencyId = agencyId;
//    }
//
//    public String getFirstName() {
//        return firstName;
//    }
//
//    public void setFirstName(String firstName) {
//        this.firstName = firstName;
//    }
//
//    public String getLastName() {
//        return lastName;
//    }
//
//    public void setLastName(String lastName) {
//        this.lastName = lastName;
//    }
//
//    public int getGantryId() {
//        return gantryId;
//    }
//
//    public void setGantryId(int gantryId) {
//        this.gantryId = gantryId;
//    }
//
//    public Date getTimestamp() {
//        return timestamp;
//    }
//
//    public void setTimestamp(Date timestamp) {
//        this.timestamp = timestamp;
//    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }
//
//    public BigDecimal getPriceCharged() {
//        return priceCharged;
//    }
//
//    public void setPriceCharged(BigDecimal priceCharged) {
//        this.priceCharged = priceCharged;
//    }


    public ProcessedHit(String direction, String callback) {
        this.direction = direction;
        this.callback = callback;
    }

    @Override
    public String toString() {
        return "ProcessedHit{" +
                "direction='" + direction + '\'' +
                ", callback='" + callback + '\'' +
                '}';
    }
}
